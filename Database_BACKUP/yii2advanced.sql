-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2017 at 07:58 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `yii2advanced`
--

-- --------------------------------------------------------

--
-- Table structure for table `dashleft`
--

CREATE TABLE `dashleft` (
  `id` int(4) NOT NULL,
  `href` varchar(150) NOT NULL,
  `content` varchar(150) NOT NULL,
  `item_bin` int(1) NOT NULL,
  `i_class` varchar(20) NOT NULL,
  `span_head` varchar(20) NOT NULL,
  `ul_class` varchar(40) NOT NULL,
  `span_head_title` varchar(40) NOT NULL,
  `sub_span` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dashleft`
--

INSERT INTO `dashleft` (`id`, `href`, `content`, `item_bin`, `i_class`, `span_head`, `ul_class`, `span_head_title`, `sub_span`) VALUES
(1, '', 'Testing', 1, 'fa fa-bar-chart-o', 'nav-label', 'nav nav-second-level collapse', '0', 'fa arrow'),
(2, 'dashboard_3.html', 'Dashboard v.3', 0, '', '', '', '1', ''),
(3, 'dashboard_4.html', 'Dashboard v.4', 0, '', '', '', '1', ''),
(4, 'dashboard_5.html', 'Dashboard v.5', 0, '', '', '', '1', ''),
(5, 'dashboard_6.html', 'Dashboard v.6', 0, '', '', '', '1', ''),
(6, 'dashboard_7.html', 'Dashboard v.7', 0, '', '', '', '1', ''),
(7, '', 'Testing2', 1, 'fa fa-bar-chart-o', 'nav-label', 'nav nav-second-level collapse', '0', 'fa arrow'),
(8, 'testing2.html', '22222222222', 0, '', '', '', '7', ''),
(9, 'dashboard8.html', 'dashboard8', 0, '', '', '', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1499764925),
('m130524_201442_init', 1499764931);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'admin', 'h2LLhhAvRlCL6aFJJVHGHnr2PXwK4uhu', '$2y$13$Q2qMcrbOe/5/j5OvGMY4W.8a2puXFM2dYivef.uVMRm/8M1/DVSea', NULL, 'admin@gmail.com', 10, 1499765263, 1499765263, ''),
(12, 'kumar', 'xXM2rpd_ibWcdc6nYfaAT5doE1sOJKyd', '$2y$13$o/7jWw4NRJiD9feq2SZXEOm2AztvTx1I2a3sMeUyZLOT73/AK6lDS', NULL, 'satheeshkumar@gmail.com', 0, 1500461412, 1500461412, ''),
(13, 'satheeshkumar', 'GkSfUXbufkVL_icVD00M6m9FQmRdYJJp', '$2y$13$BICSEy81WcqUooYXVJ.L2egchSg/rPLXqAljtZGWBGWQ0IRlubtFC', NULL, '1satheeshdragon.satheesh@gmail.com', 10, 1500461793, 1500461942, ''),
(14, 'sd', 'X90y0kyIJ1h3dm580I6qYY8sQ4t8XRuF', '$2y$13$D/anCjwX1lWUbHfGAOwiGO6Q1dxgd/IjsL2bxGoxl6Fs8zTBQb/cq', NULL, '2satheeshdragon.satheesh@gmail.com', 10, 1500462568, 1500462568, ''),
(15, 'microsoft', 'hRy2kHtFjRl9b9ZmOcDHyASVkvRDfkvU', '$2y$13$48OX3ajKpR1pYHeTbd4ICOvoHHZPqppmhlVAllpEQ8lNZ5PM152.a', NULL, 'satheeshdragon.satheesh@gmail.com', 10, 1500463616, 1500463616, ''),
(16, 'dragon', '0Qk8uA00hIA5esHKZzbyOEsejTYUkPuf', '$2y$13$cCmJcmJUbDTxO6QHC/sj3OJ4dhU53YxqBZPRWwsNWZbVanrp4xQdu', NULL, '1sd6satheeshkumar@gmail.com', 10, 1500464055, 1500464055, ''),
(17, 'google', 'uGncuArzQyUYthSG0m-46S3L04zIXZZ6', '$2y$13$qVO9gEMpu7NBMDUCTrRxAuIZkgZ4EDI.4bLja8DQkSDVXMv1Hntxu', NULL, 'satheasdeshkumar@gmail.com', 10, 1500467552, 1500467552, '2ZFyIGvFzI5jJl45DxCg7FxchqYn3gUg_1500467552'),
(18, 'ko', 'G1q1h1ivI5GcbWycASuR-0Nz8VsRU9sx', '$2y$13$8DkBTyogFA8.82WUemdUoeeSJfiM0WEEHhF.ctAB/HMX40JUj2mwC', NULL, 'satheeshasdkumar@gmail.com', 10, 1500468031, 1500468031, '-za5bZRGOKP58LI4FlXDgib7nE7629L0_1500468025'),
(19, 'ms', 'cwSoZOYGNCTVkUko6WLUoWJZnMKrJZXA', '$2y$13$M1RQJkiLNRSN2pg3b7wrrOJJ0uewNwjZkULolVu0.lm3MBKtXs8oC', NULL, '16satheeshkumar@gmail.com', 10, 1500468157, 1500468816, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dashleft`
--
ALTER TABLE `dashleft`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

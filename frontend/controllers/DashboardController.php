<?php

namespace frontend\controllers;

class DashboardController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $this->layout='dashboard';
        return $this->render('welcome');
    }

}

<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<!DOCTYPE html>
<html>
<body class="gray-bg">
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            <div class="col-md-12">
                <div class="ibox-content">
                    <h2 class="font-bold">Reset password</h2>

                    <p>
                        Please choose your new password.
                    </p>

                    <div class="row">

                        <div class="col-lg-12">
                             <?php $form = ActiveForm::begin(['class' => 'm-t'],['role' => 'form']); ?>
                                
                                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>
                                    <?= Html::submitButton('Save', ['class' => 'btn btn-primary block full-width m-b']) ?>
                                
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
               <small>© 2014-2015</small>
            </div>
        </div>
    </div>

</body>

</html>




<div class="col-md-3">
            <h2>Welcome Amelia</h2>
            <small>You have 42 messages and 6 notifications.</small>
            <ul class="list-group clear-list m-t">
                <li class="list-group-item fist-item">
                    <span class="pull-right">
                        09:00 pm
                    </span>
                    <span class="label label-success">1</span> Please contact me
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        10:16 am
                    </span>
                    <span class="label label-info">2</span> Sign a contract
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        08:22 pm
                    </span>
                    <span class="label label-primary">3</span> Open new shop
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        11:06 pm
                    </span>
                    <span class="label label-default">4</span> Call back to Sylvia
                </li>
                <li class="list-group-item">
                    <span class="pull-right">
                        12:00 am
                    </span>
                    <span class="label label-primary">5</span> Write a letter to Sandra
                </li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="flot-chart dashboard-chart">
                <div class="flot-chart-content" id="flot-dashboard-chart" style="padding: 0px; position: relative;"><canvas class="flot-base" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 631.5px; height: 180px;" width="631" height="180"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div style="position: absolute; max-width: 70px; top: 165px; left: 15px; text-align: center;" class="flot-tick-label tickLabel">0</div><div style="position: absolute; max-width: 70px; top: 165px; left: 91px; text-align: center;" class="flot-tick-label tickLabel">2</div><div style="position: absolute; max-width: 70px; top: 165px; left: 167px; text-align: center;" class="flot-tick-label tickLabel">4</div><div style="position: absolute; max-width: 70px; top: 165px; left: 243px; text-align: center;" class="flot-tick-label tickLabel">6</div><div style="position: absolute; max-width: 70px; top: 165px; left: 319px; text-align: center;" class="flot-tick-label tickLabel">8</div><div style="position: absolute; max-width: 70px; top: 165px; left: 392px; text-align: center;" class="flot-tick-label tickLabel">10</div><div style="position: absolute; max-width: 70px; top: 165px; left: 468px; text-align: center;" class="flot-tick-label tickLabel">12</div><div style="position: absolute; max-width: 70px; top: 165px; left: 544px; text-align: center;" class="flot-tick-label tickLabel">14</div><div style="position: absolute; max-width: 70px; top: 165px; left: 620px; text-align: center;" class="flot-tick-label tickLabel">16</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px;"><div style="position: absolute; top: 152px; left: 7px; text-align: right;" class="flot-tick-label tickLabel">0</div><div style="position: absolute; top: 114px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">10</div><div style="position: absolute; top: 76px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">20</div><div style="position: absolute; top: 39px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">30</div><div style="position: absolute; top: 1px; left: 1px; text-align: right;" class="flot-tick-label tickLabel">40</div></div></div><canvas class="flot-overlay" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 631.5px; height: 180px;" width="631" height="180"></canvas></div>
            </div>
            <div class="row text-left">
                <div class="col-xs-4">
                    <div class=" m-l-md">
                    <span class="h4 font-bold m-t block">$ 406,100</span>
                    <small class="text-muted m-b block">Sales marketing report</small>
                    </div>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">$ 150,401</span>
                    <small class="text-muted m-b block">Annual sales revenue</small>
                </div>
                <div class="col-xs-4">
                    <span class="h4 font-bold m-t block">$ 16,822</span>
                    <small class="text-muted m-b block">Half-year revenue margin</small>
                </div>

            </div>
        </div>
        <div class="col-md-3">
            <div class="statistic-box">
            <h4>
                Project Beta progress
            </h4>
            <p>
                You have two project with not compleated task.
            </p>
                <div class="row text-center">
                    <div class="col-lg-6"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                        <canvas id="doughnutChart2" width="80" height="80" style="margin: 18px auto 0px; display: block; width: 80px; height: 80px;"></canvas>
                        <h5>Kolter</h5>
                    </div>
                    <div class="col-lg-6"><iframe class="chartjs-hidden-iframe" style="width: 100%; display: block; border: 0px none; height: 0px; margin: 0px; position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px;"></iframe>
                        <canvas id="doughnutChart" width="80" height="80" style="margin: 18px auto 0px; display: block; width: 80px; height: 80px;"></canvas>
                        <h5>Maxtor</h5>
                    </div>
                </div>
                <div class="m-t">
                    <small>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                </div>

            </div>
        </div>


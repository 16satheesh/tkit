<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$model = $this->params['model'];
$model_register = $this->params['model_register'];
?>

<?php
$ary_home = [
        ['label' => 'home','id' =>''],
        ['label' => 'Features','id' =>'features'],
        ['label' => 'Team','id' =>'team'],
        ['label' => 'Testimonials','id' =>'testimonials'],
        ['label' => 'Pricing','id' =>'pricing'],
        ['label' => 'Contact','id' =>'contact']
    ];
$ary_main = [
        ['label' => 'about','id' =>'home/about'],
        ['label' => 'Contact','id' =>'home/contact'],
        ['label' => 'Login/SignUp','id' =>'home/login']
    ];
?>
<div class="navbar-wrapper">
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">WEBAPPLAYERS</a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        
                        <?php foreach($ary_home as $item){?>
                        <li><a class="page-scroll" href="#<?php echo $item['id'];?>"><?php echo $item['label'];?></a></li>
                        <?php }?>
                        
                        <?php foreach($ary_main as $item){
                        if($item['label']!= 'Login/SignUp'){?>
                            <li><a class="page-scroll" href="<?=$lin?><?php echo $item['id'];?>"><?php echo $item['label'];?></a></li>
                        <?php }
                        else { ?>
                            <li><a class="page-scroll" data-toggle="modal" href="#modal-form"><?php echo $item['label'];?></a></li>
                        <?php }
                         } ?>                      
                    </ul>
                </div>
            </div>
        </nav>
</div>
<div id="modal-form" class="modal fade" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6 b-r"><h3 class="m-t-none m-b">Sign in</h3>
                        <p>Sign in today for more expirience.</p>
                           <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                              <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                              <?= $form->field($model, 'password')->passwordInput() ?>
                              <?= $form->field($model, 'rememberMe')->checkbox() ?>
                              <div style="color:#999;margin:1em 0">
                                  If you forgot your password you can <?= Html::a('reset it', ['home/request-password-reset']) ?>.
                              </div>
                              <div class="form-group">
                                  <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                              </div>
                          <?php ActiveForm::end(); ?>
                    </div>
                        <div class="col-sm-6"><h4>Not a member?</h4>
                            <p>You can create an account:</p>
                                
                                <?php $form = ActiveForm::begin(['class' => 'm-t']); ?>
                                    <div class="form-group">
                                        <?= $form->field($model_register, 'username')->textInput(['autofocus' => true]) ?>
                                        
                                    </div>
                                    <div class="form-group">
                                         <?= $form->field($model_register, 'email') ?>
                                        
                                    </div>
                                    <div class="form-group">
                                        <?= $form->field($model_register, 'password')->passwordInput() ?>
                                        
                                    </div>
                                    <div class="form-group">
                                            <div class="checkbox i-checks">
                                                <label class=""> 
                                                    <div class="icheckbox_square-green" style="position: relative;">
                                                        <input style="position: absolute; " type="checkbox">
                                                        <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255) none repeat scroll 0% 0%; border: 0px none; opacity: 0;">
                                                            
                                                        </ins>
                                                    </div> Agree the terms and policy 
                                                </label>
                                            </div>
                                    </div>
                                    <?= Html::submitButton('Signup', ['class' => 'btn btn-primary block full-width m-b', 'name' => 'Register']) ?>

                                <?php ActiveForm::end(); ?>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
/**
 * @var $this yii\web\View
 * @var $user yeesoft\models\User
 */
use yii\helpers\Html;
?>
<?php
$returnUrl = Yii::$app->user->returnUrl == Yii::$app->homeUrl ? null : rtrim(Yii::$app->homeUrl, '/') . Yii::$app->user->returnUrl;
$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['home/verification', 'vToken' => $user->verification_token, 'returnUrl' => $returnUrl]);
?>

    Hello, you have been registered on 

    <br/><br/>
    Follow this link to confirm your E-mail and activate account:
   <p><?= Html::a(Html::encode($confirmLink), $confirmLink) ?></p>
